using MainLeafTest.Controller;
using MainLeafTest.Observer.Scene;
using System;
using UnityEngine;
using UnityEngine.AI;

namespace MainLeafTest.Handler
{
    [RequireComponent(typeof(NavMeshAgent), typeof(EnemyViewTrigger))]
    public class EnemyHandler : MonoBehaviour
    {
        public enum State
        {
            Warning,
            Patrolling,
            Stopping,
            FoundingPlayer,
        }

        private const float DEAD_ZONE_VELOCITY_NAV_AGENT = 0.01f;

        [Serializable]
        private struct ParticleSystemState
        {
            public float speed;
            public Color color;
        }

        #region FSM Parameters
        [Header("Walk State Parameters")]
        [SerializeField]
        private float m_walkSpeedInPatroll = 3.5f;

        [SerializeField]
        private Transform[] m_pointsPatrol;

        public float MaxSpeed { get => m_walkSpeedInPatroll; }

        private int m_currentTargetPoint = 0;

        [Header("Warning State Parameters")]
        [SerializeField]
        private float m_distanceToWarning = 10f;
        private float m_sqrDistanceToWarning;

        [SerializeField]
        private float m_walkSpeedInWarning = 1.5f;

        [Header("Attack State Parameters")]
        [SerializeField]
        private float m_distanceAttacking = 10f;
        private float m_sqrDistanceAttacking;
        #endregion

        #region State FX
        [Header("Feedback State parameters.")]
        [SerializeField]
        private ParticleSystem m_particleSystemFeedback;

        [SerializeField]
        private ParticleSystemState m_warning;

        [SerializeField]
        private ParticleSystemState m_patrolling;

        [SerializeField]
        private ParticleSystemState m_stopping;

        [SerializeField]
        private ParticleSystemState m_foundingPlayer;
        #endregion

        #region FSM State Parameters
        private State m_currentState;

        private Transform m_player;

        private NavMeshAgent m_agent;

        private EnemyViewTrigger m_enemyViewTrigger;

        private bool isTargetInFieldView = false;
        #endregion

#if UNITY_EDITOR
        #region DEBUG PARAMETERS
        [Header("DEBUG Parameters")]
        [SerializeField]
        private Mesh m_circleMesh;
        #endregion
#endif

        private void Awake()
        {
            m_agent = GetComponent<NavMeshAgent>();
            m_enemyViewTrigger = GetComponentInChildren<EnemyViewTrigger>();

            if (m_pointsPatrol != null && m_pointsPatrol.Length > m_currentTargetPoint)
            {
                m_agent.Move(m_pointsPatrol[m_currentTargetPoint].position);
            }

            InstantionObserver.RegisterCallback(
                InstantionObserver.TypeInstantiation.Player,
                SetPlayer,
                true
            );

            m_sqrDistanceToWarning = m_distanceToWarning * m_distanceToWarning;
            m_sqrDistanceAttacking = m_distanceAttacking * m_distanceAttacking;

            m_particleSystemFeedback.Play(true);

            GameplayController.Instance.OnChangingState += OnChangingGameState;
        }

        private void SetPlayer(GameObject playerGameObject)
        {
            m_player = playerGameObject.transform;
        }

        private void Update()
        {
            switch (m_currentState)
            {
                case State.Warning:
                    Warning();

                    float distToPlayer = (transform.position - m_player.position).sqrMagnitude;
                    if (m_enemyViewTrigger.IsTargetInFieldView)
                    {
                        m_currentState = State.FoundingPlayer;
                    }
                    else if (distToPlayer > m_sqrDistanceToWarning)
                    {
                        m_currentState = State.Patrolling;
                    }
                    break;

                case State.Patrolling:
                    Patrolling();

                    distToPlayer = (transform.position - m_player.position).sqrMagnitude;
                    if (m_enemyViewTrigger.IsTargetInFieldView)
                    {
                        m_currentState = State.FoundingPlayer;
                    }
                    else if (distToPlayer < m_sqrDistanceToWarning)
                    {
                        m_currentState = State.Warning;
                    }
                    break;

                case State.Stopping:
                    Stopping();

                    distToPlayer = (transform.position - m_player.position).sqrMagnitude;
                    if (m_enemyViewTrigger.IsTargetInFieldView)
                    {
                        m_currentState = State.FoundingPlayer;
                    }
                    else if (distToPlayer < m_sqrDistanceToWarning)
                    {
                        m_currentState = State.Warning;
                    }
                    else
                    {
                        m_currentState = State.Patrolling;
                    }
                    break;

                case State.FoundingPlayer:
                    FoundingPlayer();
                    break;
            }
        }

        #region State Functions
        private void Warning()
        {
            var emission = m_particleSystemFeedback.main;
            emission.startSpeed = m_warning.speed;
            emission.startColor = m_warning.color;

            emission = m_particleSystemFeedback.subEmitters.GetSubEmitterSystem(0).main;
            emission.startSpeed = m_warning.speed;


            m_agent.speed = m_walkSpeedInWarning;
            FollowPathPatroll();
        }

        private void Patrolling()
        {
            var emission = m_particleSystemFeedback.main;
            emission.startSpeed = m_patrolling.speed;
            emission.startColor = m_patrolling.color;

            emission = m_particleSystemFeedback.subEmitters.GetSubEmitterSystem(0).main;
            emission.startSpeed = m_patrolling.speed;

            m_agent.speed = m_walkSpeedInPatroll;
            FollowPathPatroll();
        }

        private void Stopping()
        {
            var emission = m_particleSystemFeedback.main;
            emission.startSpeed = m_stopping.speed;
            emission.startColor = m_stopping.color;

            emission = m_particleSystemFeedback.subEmitters.GetSubEmitterSystem(0).main;
            emission.startSpeed = m_stopping.speed;

            m_agent.isStopped = true;
        }

        private void FoundingPlayer()
        {
            var emission = m_particleSystemFeedback.main;
            emission.startSpeed = m_foundingPlayer.speed;
            emission.startColor = m_foundingPlayer.color;

            emission = m_particleSystemFeedback.subEmitters.GetSubEmitterSystem(0).main;
            emission.startSpeed = m_foundingPlayer.speed;

            m_agent.isStopped = true;
            GameplayController.Instance.EnemyFoundPlayer(this);
        }
        #endregion

        private void FollowPathPatroll()
        {
            if (m_agent.remainingDistance <= m_agent.stoppingDistance)
            {
                m_currentTargetPoint++;
                if (m_currentTargetPoint >= m_pointsPatrol.Length)
                {
                    m_currentTargetPoint = 0;
                }

                m_agent.isStopped = false;
                m_agent.SetDestination(m_pointsPatrol[m_currentTargetPoint].position);
            }

            if (m_agent.isStopped)
            {
                m_agent.isStopped = false;
                m_agent.SetDestination(m_pointsPatrol[m_currentTargetPoint].position);
            }
        }

        public void OnChangingGameState(GameplayController.GameplayState gameState)
        {
            if (gameState == GameplayController.GameplayState.Pausing)
            {
                enabled = false;
                m_agent.isStopped = true;
                m_particleSystemFeedback.Pause(true);
            }
            else if (gameState == GameplayController.GameplayState.Playing)
            {
                enabled = true;
                m_agent.isStopped = false;
                m_particleSystemFeedback.Play(true);
            }
        }

        private void OnDestroy()
        {
            InstantionObserver.UnregisterCallback(
                InstantionObserver.TypeInstantiation.Player,
                SetPlayer
            );
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            if (m_pointsPatrol == null || m_pointsPatrol.Length < 2) return;

            var cacheColor = Gizmos.color;
            Gizmos.color = Color.green;

            Gizmos.DrawLine(m_pointsPatrol[0].position, m_pointsPatrol[m_pointsPatrol.Length - 1].position);
            for (int i = 0; i < m_pointsPatrol.Length - 1; i++)
            {
                Gizmos.DrawLine(m_pointsPatrol[i].position, m_pointsPatrol[i + 1].position);
            }

            Gizmos.color = Color.red;
            Gizmos.DrawWireMesh(m_circleMesh, transform.position, transform.rotation, new Vector3(m_distanceAttacking, m_distanceAttacking, m_distanceAttacking));

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireMesh(m_circleMesh, transform.position, transform.rotation, new Vector3(m_distanceToWarning, m_distanceAttacking, m_distanceToWarning));

            Gizmos.color = cacheColor;
        }
#endif
    }
}