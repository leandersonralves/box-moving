﻿using MainLeafTest.Controller;
using MainLeafTest.Handler;
using UnityEngine;
using UnityEngine.AI;

using GameState = MainLeafTest.Controller.GameplayController.GameplayState;

[RequireComponent(typeof(Animator), typeof(NavMeshAgent))]
public class EnemyAnimatorRefresh : MonoBehaviour
{
    private Animator m_animator;

    private NavMeshAgent m_navMeshAgent;

    private EnemyHandler m_enemyHandler;

    private string m_fowardKeyAnimator = "Forward";

    private int m_forwardHashAnimator;

    private string m_turnKeyAnimator = "Turn";

    private int m_turnHashAnimator;

    void Awake()
    {
        m_navMeshAgent = GetComponent<NavMeshAgent>();
        m_animator = GetComponent<Animator>();
        m_enemyHandler = GetComponent<EnemyHandler>();

        m_forwardHashAnimator = Animator.StringToHash(m_fowardKeyAnimator);
        m_turnHashAnimator = Animator.StringToHash(m_turnKeyAnimator);

        GameplayController.Instance.OnChangingState += OnChangingGameState;
    }

    // Update is called once per frame
    void Update()
    {
        m_animator.SetFloat(m_forwardHashAnimator, m_navMeshAgent.velocity.magnitude / m_enemyHandler.MaxSpeed);
        //m_animator.SetFloat(m_turnHashAnimator, m_navMeshAgent.steeringTarget.y / m_enemyHandler.MaxSpeed);
    }

    public void OnChangingGameState (GameState gameState)
    {
        if (gameState == GameplayController.GameplayState.Pausing)
        {
            m_animator.speed = 0f;
        }
        else if (gameState == GameplayController.GameplayState.Playing)
        {
            m_animator.speed = 1f;
        }
    }
}
