﻿using UnityEngine;

namespace MainLeafTest.Handler
{
    public class BoxHandler : MonoBehaviour
    {
        [SerializeField]
        private Transform[] m_pointsPush;

        private bool m_isBeingHolding = false;

        private Transform m_handHolding;

        private Vector3 m_offsetHand;

        private Rigidbody m_rigibody;

        private void Awake()
        {
            m_rigibody = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            if (m_isBeingHolding)
            {
                m_rigibody.MovePosition(m_handHolding.position + m_offsetHand);
            }
        }

        public void HoldingBy(Transform handHolding)
        {
            m_handHolding = handHolding;
            m_isBeingHolding = true;
            m_offsetHand = transform.position - handHolding.position;
        }

        public void ReleaseByHand(Transform handReleasing)
        {
            if (m_isBeingHolding && m_handHolding.Equals(handReleasing))
            {
                m_isBeingHolding = false;
                m_handHolding = null;
                m_offsetHand = Vector3.zero;
            }
        }

        public Transform GetNearestPointPush (Vector3 position)
        {
            float nearesDist = float.MaxValue;
            float currentDist = 0f;
            Transform nearestPosition = null;
            foreach(var p in m_pointsPush)
            {
                currentDist = (p.position - position).sqrMagnitude;
                if (currentDist < nearesDist)
                {
                    nearesDist = currentDist;
                    nearestPosition = p;
                }
            }

            return nearestPosition;
        }
    }
}