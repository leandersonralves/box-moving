﻿using System.Collections;
using MainLeafTest.Observer;
using MainLeafTest.Observer.Scene;
using UnityEngine;
using GameState = MainLeafTest.Controller.GameplayController.GameplayState;

namespace MainLeafTest.Handler
{
    /// <summary>
    /// Class to read inputs and state Character and pass to Animator.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class CharacterHandler : MonoBehaviour
    {
        #region Animator Components and parameters.
        private Animator m_animator;

        //TODO: agregate key name with hash in a struct.
        private string m_fowardKeyAnimator = "Forward";
        private int m_forwardHash;

        private string m_strafeKeyAnimator = "Turn";
        private int m_strafeHash;

        private string m_crouchKeyAnimator = "Crouch";
        private int m_crouchHash;

        private string m_carryingBoxKeyAnimator = "CarryingBox";
        private int m_carryingBoxHash;

        private string m_groundHitKeyAnimator = "OnGround";
        private int m_groundHitHash;

        private string m_jumpKeyAnimator = "Jump";
        private int m_jumpHash;
        #endregion

        #region Interaction Objects
        private BoxHandler nearestBoxHandler;

        private BoxObserver m_boxObserver;

        public bool IsCarryingBox { get; private set; }
        #endregion

        #region Push/Pull Parameters
        [SerializeField]
        private Transform handBone;
        private const float DEAD_POSITION = 0.1f;
        private bool m_isRepositioningPushPull = false;
        #endregion

        #region Jump parameters
        [Header("Jump Mecanic Parameters")]
        [SerializeField]
        private float m_jumpForceForward;

        [SerializeField]
        private float m_jumpForceUp;

        [SerializeField]
        private float m_pointACheckHitGround = 1.5f;

        [SerializeField]
        private float m_pointBCheckHitGround = 1.75f;

        [SerializeField]
        private LayerMask layerMaskGroundHit;

        private float m_periodCheckGround = 1f;

        public bool OnGround { get; private set; } = true;

        private Rigidbody m_rigidbody;
        #endregion

        #region Movement Parameters
        [Header("Movement Parameters")]
        [SerializeField]
        private float m_speedAlignForward = 10f;

        private bool m_newForceForward = false;

        private Vector3 m_newDirectionForward;

        public float FowardAngle { get => 0; }//IsAligningMovement ? Vector3.Angle(Vector3.forward, m_forwardToRealign) : transform.eulerAngles.y; }

        private float v = 0f;

        private float h = 0f;

        public bool IsWalking { get => !Mathf.Approximately(v, 0f); }

        private Vector3 m_startPosition;

        private Quaternion m_startRotation;
        #endregion

        void Awake()
        {
            InstantionObserver.TriggerCallback(InstantionObserver.TypeInstantiation.Player, gameObject);

            m_animator = GetComponent<Animator>();
            m_rigidbody = GetComponent<Rigidbody>();

            //reading Animator hashs
            m_forwardHash = Animator.StringToHash(m_fowardKeyAnimator);
            m_strafeHash = Animator.StringToHash(m_strafeKeyAnimator);
            m_crouchHash = Animator.StringToHash(m_crouchKeyAnimator);
            m_carryingBoxHash = Animator.StringToHash(m_carryingBoxKeyAnimator);
            m_groundHitHash = Animator.StringToHash(m_groundHitKeyAnimator);
            m_jumpHash = Animator.StringToHash(m_jumpKeyAnimator);

            if (TryGetComponent<BoxObserver>(out m_boxObserver))
            {
                m_boxObserver.onChangeState += SetBoxNextToCharacter;
            }
            else
            {
                Debug.LogWarning("Character (" + transform.root.name + ") will not Interact with Box, please add BoxObserver.");
            }

            m_newDirectionForward = transform.forward;

            Controller.GameplayController.Instance.OnChangingState += OnChangingGameState;

            m_startPosition = transform.position;
            m_startRotation = transform.rotation;
        }

        void Update()
        {
            v = Input.GetAxis("Vertical");
            h = Input.GetAxis("Horizontal");

            if (OnGround)
            {
                bool isCrouched = Input.GetButton("Crouch");
                bool isInteracting = Input.GetButton("Interact");
                bool isDownInteracting = Input.GetButtonDown("Interact");

                m_animator.SetFloat(m_forwardHash, v);
                m_animator.SetFloat(m_strafeHash, h);
                m_animator.SetBool(m_crouchHash, isCrouched);

                if (isInteracting)
                {
                    if (nearestBoxHandler && isDownInteracting && !m_isRepositioningPushPull)
                    {
                        //Positioning the character for the push/pull position.
                        StartCoroutine(
                            PositionToPush(nearestBoxHandler)
                        );
                    }
                }
                else if (nearestBoxHandler)
                {
                    nearestBoxHandler.ReleaseByHand(handBone);
                }

                //checking if there is a box next to Character and Button Interact is pressed.
                IsCarryingBox = isInteracting && nearestBoxHandler;
                if (!IsCarryingBox)
                {
                    if (Mathf.Sign(v) == 1f && Input.GetButtonDown("Jump"))
                    {
                        Debug.Log("Jumping!");
                        StartCoroutine(Jump());
                    }

                    m_animator.SetBool(m_carryingBoxHash, IsCarryingBox);
                }
            }
            else
            {
                m_animator.SetFloat(m_jumpHash, m_rigidbody.velocity.y);
                m_rigidbody.AddForce(transform.forward * m_jumpForceForward * v, ForceMode.Impulse);
                IsCarryingBox = false;
            }

            m_animator.SetBool(m_groundHitHash, OnGround);
        }

        private IEnumerator Jump ()
        {
            OnGround = false;
            m_rigidbody.AddForce(Vector3.up * m_jumpForceUp + m_rigidbody.velocity, ForceMode.Impulse);

            //delay to check if is grounded.
            yield return new WaitForSeconds(m_periodCheckGround);

            RaycastHit hitInfo;

            while(!OnGround)
            {
                Vector3 p1 = transform.position + Vector3.up * m_pointACheckHitGround;
                Vector3 p2 = transform.position + Vector3.down * m_pointBCheckHitGround;
                OnGround = Physics.Linecast(p1, p2, out hitInfo, layerMaskGroundHit);
                if (OnGround) Debug.Log("Hitted " + hitInfo.transform.name);
                yield return null;
            }
        }

        /// <summary>
        /// Aproximate Character to nearest push/pull position of Box of <paramref name="nearestBoxHandler"/>.
        /// </summary>
        /// <param name="nearestBoxHandler"></param>
        /// <returns></returns>
        private IEnumerator PositionToPush(BoxHandler nearestBoxHandler)
        {
            m_isRepositioningPushPull = true;
            Debug.Log("Repositioning next push/pull Position at " + nearestBoxHandler.name);
            Transform p2 = nearestBoxHandler.GetNearestPointPush(transform.position);
            Vector3 p1 = transform.position;
            float t = 0f;
            float sqrdDist = (p2.position - p1).sqrMagnitude;
            transform.forward = p2.forward;

            while (sqrdDist > DEAD_POSITION)
            {
                transform.position = Vector3.Lerp(p1, p2.position, t);
                t += Time.deltaTime;
                sqrdDist = (p2.position - transform.position).sqrMagnitude;
                Debug.Log("Distante Squared to box " + sqrdDist);
                yield return null;
            }

            transform.position = p2.position;
            nearestBoxHandler.HoldingBy(handBone);

            m_animator.SetBool(m_carryingBoxHash, true);
            m_isRepositioningPushPull = false;
        }

        /// <summary>
        /// Define a Box next to Character, and this character can hold/push/pull.
        /// </summary>
        /// <param name="nearestBox"></param>
        private void SetBoxNextToCharacter(Transform nearestBox)
        {
            if (nearestBox)
            {
                this.nearestBoxHandler = nearestBox.GetComponentInParent<BoxHandler>();
                Controller.UIController.Instance.ShowActionButton(true);
            }
            else
            {
                this.nearestBoxHandler.ReleaseByHand(transform);
                this.nearestBoxHandler = null;
                Controller.UIController.Instance.ShowActionButton(false);
            }
        }

        private void OnDrawGizmosSelected()
        {
            var cacheColor = Gizmos.color;
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(
                transform.position + Vector3.up * m_pointACheckHitGround,
                transform.position + Vector3.down * m_pointBCheckHitGround
            );
            Gizmos.color = cacheColor;
        }

        /// <summary>
        /// Force a new Forward direction to Align as character walks.
        /// </summary>
        /// <param name="newDirectionForward"></param>
        public void ForceFowardDirection (Vector3 newDirectionForward)
        {
            if (!m_newForceForward)
            {
                StartCoroutine(AligningToNewForward());
            }
            m_newDirectionForward = newDirectionForward;
        }

        /// <summary>
        /// Align forward of Character to <paramref name="m_newDirectionForward"/>.
        /// </summary>
        /// <returns></returns>
        IEnumerator AligningToNewForward ()
        {
            m_newForceForward = true;

            m_newDirectionForward.Normalize();

            float dot =  float.MaxValue;
            while(!Mathf.Approximately(dot, 0f))
            {
                if (IsWalking && OnGround && !IsCarryingBox)
                {
                    Debug.Log("forward " + transform.forward);
                    Debug.Log("new forward " + m_newDirectionForward);
                    transform.forward = Vector3.Lerp(transform.forward, m_newDirectionForward, Time.deltaTime * m_speedAlignForward);
                    dot = (m_newDirectionForward - transform.forward).sqrMagnitude;
                }

                yield return null;
            }
            m_newForceForward = false;
        }

        public void OnChangingGameState (GameState gameState)
        {
            switch(gameState)
            {
                case GameState.Pausing:
                    m_animator.speed = 0f;
                    enabled = false;
                    break;

                case GameState.Playing:
                    m_animator.speed = 1f;
                    enabled = true;
                    break;

                case GameState.Loosing:
                case GameState.Restarting:
                    transform.position = m_startPosition;
                    transform.rotation = m_startRotation;
                    enabled = false;
                    break;
            }
        }
    }
}