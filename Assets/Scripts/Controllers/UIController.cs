﻿using UnityEngine;
using UnityEngine.UI;
using GameplayState = MainLeafTest.Controller.GameplayController.GameplayState;

namespace MainLeafTest.Controller
{
    public class UIController : MonoBehaviour
    {
        [Header("Multiples Canvas (Lose, Win, Pause and Gameplay).")]
        [SerializeField]
        private GameObject m_screenLose;

        [SerializeField]
        private GameObject m_screenWin;

        [SerializeField]
        private GameObject m_screenPause;

        [SerializeField]
        private GameObject m_screenGameplay;


        [Header("Gameplay Canvas components.")]
        [SerializeField]
        private Image m_actionImage;

        [SerializeField]
        private Sprite m_spriteMouseLeft;

        [SerializeField]
        private TMPro.TextMeshProUGUI m_textCoins;

        private int m_cacheAmountCoin = int.MaxValue;
        private Color m_textCoinCacheColor;

        public static UIController Instance { get; private set; }

        private void Awake()
        {
            if (Instance) { Destroy(gameObject); }
            Instance = this;

            m_textCoinCacheColor = m_textCoins.color;
        }

        void Start()
        {
            GameplayController.Instance.OnChangingState += OnChangeGameState;
            OnChangeGameState(GameplayController.Instance.CurrentState);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                switch (GameplayController.Instance.CurrentState)
                {
                    case GameplayState.Pausing:
                        PauseGame(false);
                        break;
                    case GameplayState.Playing:
                        PauseGame(true);
                        break;
                }
            }
        }

        private void FixedUpdate()
        {
            if (m_cacheAmountCoin != GameplayController.Instance.PlayerData.amountCoins)
            {
                m_cacheAmountCoin = GameplayController.Instance.PlayerData.amountCoins;
                m_textCoins.text = GameplayController.Instance.PlayerData.amountCoins.ToString("00");
                m_textCoins.CrossFadeColor(Color.green, 1f, false, false);
                m_textCoins.CrossFadeColor(m_textCoinCacheColor, 3f, false, false);
            }
        }

        public void PauseGame(bool isPaused)
        {
            if (!isPaused)
            {
                GameplayController.Instance.ChangeState(GameplayState.Playing);
            }
            else if (isPaused)
            {
                GameplayController.Instance.ChangeState(GameplayState.Pausing);
            }
        }

        public void OnChangeGameState(GameplayState newState)
        {
            switch (newState)
            {
                case GameplayState.Playing:
                    m_screenGameplay.SetActive(true);
                    m_screenPause.SetActive(false);
                    m_screenLose.SetActive(false);
                    m_screenWin.SetActive(false);
                    Cursor.lockState = CursorLockMode.Locked;
                    break;

                case GameplayState.Pausing:
                    m_screenGameplay.SetActive(false);
                    m_screenPause.SetActive(true);
                    m_screenLose.SetActive(false);
                    m_screenWin.SetActive(false);
                    Cursor.lockState = CursorLockMode.None;
                    break;

                case GameplayState.Loosing:
                    m_screenGameplay.SetActive(false);
                    m_screenPause.SetActive(false);
                    m_screenLose.SetActive(true);
                    m_screenWin.SetActive(false);
                    Cursor.lockState = CursorLockMode.None;
                    break;

                case GameplayState.Wining:
                    m_screenGameplay.SetActive(false);
                    m_screenPause.SetActive(false);
                    m_screenLose.SetActive(false);
                    m_screenWin.SetActive(true);
                    Cursor.lockState = CursorLockMode.None;
                    break;
            }
        }

        public void ShowActionButton(bool isShowing)
        {
            m_actionImage.sprite = isShowing ? m_spriteMouseLeft : null;
            m_actionImage.gameObject.SetActive(isShowing);
        }

        public void NewGame()
        {
            GameplayController.Instance.NewGame();
        }

        public void RestarGame()
        {
            if (GameplayController.Instance.CurrentState != GameplayState.Restarting)
            {
                GameplayController.Instance.Restart();
            }
        }

        public void QuitGame()
        {
            GameplayController.Instance.QuitGame();
        }
    }
}