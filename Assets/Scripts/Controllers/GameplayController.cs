﻿using MainLeafTest.Handler;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MainLeafTest.Controller
{
    public class GameplayController : MonoBehaviour
    {
        private static GameplayController m_instance;

        private Data.PlayerData m_playerData;

        public Data.PlayerData PlayerData { get => m_playerData; }

        public static GameplayController Instance
        {
            get
            {
                if (!m_instance)
                {
                    var go = new GameObject("GameplayController - created by script", typeof(GameplayController));
                    m_instance = go.GetComponent<GameplayController>();
                }

                return m_instance;
            }
        }

        public enum GameplayState
        {
            Restarting,
            Pausing,
            Playing,
            Loosing,
            Wining
        }

        public GameplayState CurrentState { get; private set; } = GameplayState.Playing;

        public Action<GameplayState> OnChangingState;

        private CharacterHandler m_playerHandler;

        private Queue<Tuple<float, GameplayState>> m_stateQueue = new Queue<Tuple<float, GameplayState>>();

        private void Awake()
        {
            if (m_instance)
            {
                Destroy(this);
                return;
            }

            m_instance = this;
        }

        IEnumerator Start() {
            GameObject playerGameObject = GameObject.FindWithTag("Player");
            while(!playerGameObject)
            {
                playerGameObject = GameObject.FindWithTag("Player");
                m_playerHandler = playerGameObject.GetComponent<CharacterHandler>();
                yield return null;
            }
        }

        private void Update() {
            if (m_stateQueue.Count > 0)
            {
                var nextState = m_stateQueue.Peek();
                if (nextState.Item1 <= Time.unscaledTime)
                {
                    m_stateQueue.Dequeue();
                    ChangeState(GameplayState.Playing);
                }
            }
        }

        public void ChangeState(GameplayState newState)
        {
            CurrentState = newState;
            OnChangingState?.Invoke(newState);
        }

        public void EnemyFoundPlayer(EnemyHandler enemyHandler)
        {
            ChangeState(GameplayState.Loosing);
        }

        public void EnemyFallWater ()
        {
            if (CurrentState != GameplayState.Loosing)
            {
                ChangeState(GameplayState.Loosing);
            }
        }

        public void SumAmountCoins (int amountCoins)
        {
            m_playerData.amountCoins += amountCoins;
        }

        public void NewGame ()
        {
            var currentLevel = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentLevel.name);
        }

        public void Pause (bool isPaused)
        {
            if (!isPaused)
            {
            }
        }

        public void Restart()
        {
            ChangeState(GameplayState.Restarting);
            m_stateQueue.Enqueue(
                new Tuple<float, GameplayState>(Time.unscaledTime + 0.5f, GameplayState.Playing)
            ); //enqueuing Playing State after 2 sec.
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}