using System;
using UnityEngine;
using UnityEngine.Profiling;

public class EnemyViewTrigger : MonoBehaviour
{
    [SerializeField]
    private string tagObjectTrigger = "Player";

    [SerializeField]
    private float m_fieldOfView = 60f;

    [SerializeField]
    private float m_distanceView = 5f;

    [SerializeField]
    private float m_metersPrecision = 1f;

    [SerializeField]
    private float m_heightFromY0 = 1.5f;

    [SerializeField]
    private MeshFilter m_meshFieldOfViewFilter;

    [SerializeField]
    private LayerMask m_hitLayers;

    private bool isTargetTriggeredCollider = false;

    public bool IsTargetInFieldView { get; private set; }

    public Action<bool> OnObjectVisualized;

    private SphereCollider m_collider;

    private Collider m_target;

    private FieldOfViewMesh m_fovMesh;

    private void Awake()
    {
        //Using SphereCollider, because Unity (Physics in really) doesn't support Trigger/Collision with non-convex mesh.
        if (!gameObject.TryGetComponent<SphereCollider>(out m_collider))
        {
            m_collider = gameObject.AddComponent<SphereCollider>();
        }
        m_collider.radius = m_distanceView;
        m_collider.isTrigger = true;

        m_fovMesh = new FieldOfViewMesh(m_fieldOfView, m_distanceView, m_metersPrecision, m_heightFromY0);
        m_meshFieldOfViewFilter.mesh = m_fovMesh;
    }

    private void FixedUpdate()
    {
        //Profiler.BeginSample("FixedUpdate view enemy raycast.");

        RaycastHit rayCastInfo;
        for (int i = 1; i < m_fovMesh.Length; i++)
        {
            Vector3 vertWorld = transform.localToWorldMatrix.MultiplyPoint3x4(m_fovMesh.GetRealVertice(i));

            if (Physics.Linecast(transform.position, vertWorld, out rayCastInfo, m_hitLayers.value))
            {
                m_fovMesh.SetVertice(i, m_fovMesh.GetRealVertice(i).normalized * rayCastInfo.distance);
            }
        }

        m_fovMesh.FlushVertices();

        if (isTargetTriggeredCollider)
        {
            var localPosition = transform.worldToLocalMatrix.MultiplyPoint3x4(m_target.transform.position);
            if (m_fovMesh.InFieldOfView(localPosition))
            {
                if (Physics.Linecast(
                    transform.position + Vector3.up * m_heightFromY0,
                    m_target.transform.position + Vector3.up * m_heightFromY0,
                    out rayCastInfo,
                    m_hitLayers
                ))
                {
                    IsTargetInFieldView = rayCastInfo.collider.Equals(m_target);
                    if (IsTargetInFieldView)
                    {
                        OnObjectVisualized?.Invoke(true);
                    }
                }
            }
        }

        //Profiler.EndSample();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(tagObjectTrigger))
        {
            m_target = other;
            isTargetTriggeredCollider = true;
            Debug.Log("Object (" + other.name + ") In of View ");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(tagObjectTrigger))
        {
            m_target = null;
            isTargetTriggeredCollider = false;
            OnObjectVisualized?.Invoke(false);
            Debug.Log("Object (" + other.name + ") Out of View ");
        }
    }
}
