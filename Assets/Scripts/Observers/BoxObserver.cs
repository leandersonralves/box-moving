﻿using System;
using UnityEngine;

namespace MainLeafTest.Observer
{
    /// <summary>
    /// This class trigger callback when your Collider trigger other with tag "InteractableBox".
    /// </summary>
    public class BoxObserver : MonoBehaviour
    {
        /// <summary>
        /// Callback called when in triggered.
        /// If is enter state argument bool is True, False if is exiting.
        /// </summary>
        public Action<Transform> onChangeState;

        private const string BOX_TAG = "InteractableBox";

        private Transform m_nearestBox;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(BOX_TAG))
            {
                bool canCarry = true;

                //Check if there is another Box near and calculating nearest.
                if (m_nearestBox != null)
                {
                    var sqrdDistToNew = (transform.position - other.transform.position).sqrMagnitude;
                    var sqrdDistToOld = (transform.position - m_nearestBox.position).sqrMagnitude;
                    canCarry = sqrdDistToNew < sqrdDistToOld;
                }

                if (canCarry)
                {
                    m_nearestBox = other.transform;
                    onChangeState?.Invoke(m_nearestBox);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(BOX_TAG) && m_nearestBox.Equals(other.transform))
            {
                m_nearestBox = null;
                onChangeState?.Invoke(null);
            }
        }
    }
}