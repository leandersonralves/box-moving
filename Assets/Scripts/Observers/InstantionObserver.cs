﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MainLeafTest.Observer.Scene
{
    /// <summary>
    /// Class to check instantion of some Components.
    /// Can be used to watch already objects instantiated.
    /// TODO: implement a event instantiation type-oriented, type-oriented is a more robust and bug-free method instead use Enum.
    /// </summary>
    public static class InstantionObserver
    {
        public enum TypeInstantiation
        {
            Camera,
            Player,
        }

        private static Dictionary<TypeInstantiation, Action<GameObject>> m_callbacks = new Dictionary<TypeInstantiation, Action<GameObject>>();

        private static Dictionary<TypeInstantiation, GameObject> m_callbacksExecuted = new Dictionary<TypeInstantiation, GameObject>();



        public static void RegisterCallback (TypeInstantiation typeInstantiation, Action<GameObject> callback, bool checkBuffer = false)
        {
            Debug.Log("Registering callback " + typeInstantiation);

            if (!m_callbacks.ContainsKey(typeInstantiation)) m_callbacks.Add(typeInstantiation, null);

            if (checkBuffer && m_callbacksExecuted.ContainsKey(typeInstantiation))
            {
                callback.Invoke(m_callbacksExecuted[typeInstantiation]);
            }

            m_callbacks[typeInstantiation] += callback;
        }

        public static void TriggerCallback (TypeInstantiation typeInstantiation, GameObject gameObject)
        {
            Debug.Log("Triggering callback " + typeInstantiation);

            m_callbacksExecuted[typeInstantiation] = gameObject;
            if (m_callbacks.ContainsKey(typeInstantiation))
            {
                m_callbacks[typeInstantiation].Invoke(gameObject);
            }
        }

        public static void UnregisterCallback (TypeInstantiation typeInstantiation, Action<GameObject> callback)
        {
            if (m_callbacks.ContainsKey(typeInstantiation))
            {
                m_callbacks[typeInstantiation] -= callback;
            }
        }
    }
}
