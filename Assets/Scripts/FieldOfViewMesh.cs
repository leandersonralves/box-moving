﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class FieldOfViewMesh
{
    private Mesh m_mesh = new Mesh();

    private float m_fieldOfView;

    private float m_distanceView;

    private float m_sqrdistanceView;

    private float m_metersPrecision;

    private float m_heightFromY0;

    private Vector3[] vertices = new Vector3[0];

    private Vector3[] originVertices = new Vector3[0];

    private Queue<int> m_verticesToRefresh = new Queue<int>();

    private int[] indexes = new int[0];

    private bool hasChanged = false;

    public int Length { get; private set; }

    public FieldOfViewMesh (float fieldOfView, float distanceView, float metersPrecision, float heightFromY0)
    {
        m_fieldOfView = fieldOfView;
        m_distanceView = distanceView;
        m_metersPrecision = metersPrecision;
        m_heightFromY0 = heightFromY0;

        m_sqrdistanceView = distanceView * distanceView;

        CreatePrismMesh(fieldOfView, distanceView, metersPrecision, heightFromY0);
    }

    private void CreatePrismMesh(float fieldOfView, float distanceView, float metersPrecision, float heightFromY0)
    {
        //TODO: optimize mesh creation, 'cause all segments are connected to once vertex at origin.
        //Pitagoras theorem
        float anglePrecision = Mathf.Rad2Deg * Mathf.Asin(metersPrecision / distanceView); //angle of cosine, metersPrecision is half, to calculate as a right angle.

        Length = (int)(fieldOfView / anglePrecision) + 2;

        if (Length < 3)
        {
            throw new Exception("Trying create a strange Field Of View Mesh, try use different angle of field of view (" + fieldOfView + ") and/or meter precision (" + metersPrecision + ")." );
        }

        vertices = new Vector3[Length];
        originVertices = new Vector3[Length];

        vertices[0] = new Vector3(0f, heightFromY0, 0f);
        originVertices[0] = new Vector3(0f, heightFromY0, 0f);

        indexes = new int[Length * 3];

        float angle = -fieldOfView * .5f;

        for (int i = 1; i < Length; i++)
        {
            vertices[i] = Quaternion.Euler(0f, angle, 0f) * (Vector3.forward * distanceView + Vector3.up * heightFromY0);
            originVertices[i] = Quaternion.Euler(0f, angle, 0f) * (Vector3.forward * distanceView + Vector3.up * heightFromY0);
            //Debug.Log("ang " + angle);
            angle += anglePrecision;
            //Debug.Log("vert " + vertices[i]);
            if (i > 1)
            {
                indexes[i * 3 + 1] = 0;
                indexes[i * 3] = i;
                indexes[i * 3 - 1] = i - 2;
                // Debug.Log("ind 0 " + indexes[i - 2]);
                // Debug.Log("ind 1 " + indexes[i - 1]);
                // Debug.Log("ind 2 " + indexes[i]);
            }
        }

        m_mesh.Clear();
        m_mesh.SetVertices(vertices);
        m_mesh.SetTriangles(indexes, 0, true);
    }

    public void SetVertice(int index, Vector3 vertice)
    {
        vertices[index] = vertice;
        m_verticesToRefresh.Enqueue(index);
        hasChanged = true;
    }

    public Vector3 GetVertice (int index)
    {
        return hasChanged ? vertices[index] : originVertices[index];
    }

    public Vector3 GetRealVertice (int index)
    {
        return originVertices[index];
    }

    public void FlushVertices()
    {
        if (hasChanged)
        {
            m_mesh.SetVertices(vertices);
            Array.Copy(originVertices, vertices, originVertices.Length);
        }
        else
        {
            m_mesh.SetVertices(originVertices);
        }
    }

    public void ResetMesh()
    {
        if (!hasChanged) return;

        hasChanged = false;
        m_mesh.SetVertices(originVertices);
    }

    /// <summary>
    /// Check if a position in inner of FOV.
    /// </summary>
    /// <param name="localPosition">Local Position of transform who own this Mesh.</param>
    public bool InFieldOfView(Vector3 localPosition)
    {
        if (localPosition.sqrMagnitude > m_sqrdistanceView)
        {
            return false;
        }

        float ang = Vector3.SignedAngle(originVertices[1], localPosition, Vector3.up);
        if (ang < 0)
        {
            ang = 360f + ang;
        }

        return ang < m_fieldOfView;
    }

    public static implicit operator Mesh(FieldOfViewMesh fovMesh) => fovMesh.m_mesh;
}
