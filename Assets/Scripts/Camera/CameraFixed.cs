﻿using System.Collections;
using MainLeafTest.Handler;
using MainLeafTest.Observer.Scene;
using UnityEngine;

namespace MainLeafTest.Camera
{
    public class CameraFixed : MonoBehaviour
    {
        #region Translate Parameters
        private const float DEAD_ZONE_TRANSLATE = 0.01f;

        [Header("Translate Parameters")]
        [SerializeField]
        private float m_maxTranslaterVelocity = 20f;

        [SerializeField]
        private float m_translateDelayTime = 10f;
        #endregion

        #region  Angular Parameters
        [Header("Angular Parameters")]
        [SerializeField]
        private float m_offsetEyeHeight = 10f;

        [SerializeField]
        private float m_maxAngularVelocity = 20f;

        [SerializeField]
        private float m_AngularDelayTime = 10f;

        [SerializeField]
        private float m_characterRotationSpeed = 200f;

        #endregion

        #region References Variables
        private Transform m_target;

        private CharacterHandler m_targetCharacterHandler;

        private Vector3 m_currentTranslateVelocity;

        private Vector3 m_currentAngularVelocity;

        private Vector3 m_newPosition;

        private float accumulatedMouseX = 0f;
        #endregion

        private void Awake()
        {
            InstantionObserver.RegisterCallback(
                InstantionObserver.TypeInstantiation.Player,
                SetTarget,
                true
            );
        }

        void Update()
        {
            if (!m_target) return;

            Vector3 targetDirection = (m_target.position - transform.position) + new Vector3(0f, m_offsetEyeHeight, 0f);
            transform.forward = Vector3.SmoothDamp(
                transform.forward, targetDirection, ref m_currentAngularVelocity, m_AngularDelayTime, m_maxAngularVelocity, Time.deltaTime
            );

            float mouseX = Input.GetAxis("Mouse X");
            if (!Mathf.Approximately(0f, mouseX))
            {
                accumulatedMouseX += mouseX * m_characterRotationSpeed * Time.deltaTime;
                var newDirection = Quaternion.Euler(0f, accumulatedMouseX, 0f) * Vector3.forward;
                m_targetCharacterHandler.ForceFowardDirection(newDirection);
            }
        }

        IEnumerator Translating()
        {
            float sqrDistante = (transform.position - m_newPosition).sqrMagnitude;
            while (sqrDistante > DEAD_ZONE_TRANSLATE)
            {
                transform.position = Vector3.SmoothDamp(
                    transform.position, m_newPosition, ref m_currentTranslateVelocity, m_translateDelayTime, m_maxTranslaterVelocity, Time.deltaTime
                );
                yield return null;
            }
            transform.position = m_newPosition;
        }



        public void SetTarget(GameObject newTarget)
        {
            m_target = newTarget.transform;
            m_targetCharacterHandler = m_target.GetComponent<Handler.CharacterHandler>();
        }

        public void SetNewFixedPosition(Vector3 newPosition)
        {
            this.m_newPosition = newPosition;
            StartCoroutine(Translating());
        }

        private void OnDestroy()
        {
            InstantionObserver.UnregisterCallback(
                InstantionObserver.TypeInstantiation.Player,
                SetTarget
            );
        }
    }
}