﻿using MainLeafTest.Handler;
using MainLeafTest.Observer.Scene;
using UnityEngine;

namespace MainLeafTest.Camera.Trigger
{
    public class TriggerCameraState : MonoBehaviour
    {
        [SerializeField]
        private string m_tagGameObjectTriggering = "Player";

        [SerializeField]
        private CameraHandler.State m_newState = CameraHandler.State.Follow;

        private CameraHandler m_cameraHandler = null;

        private void Awake()
        {
            InstantionObserver.RegisterCallback(
                InstantionObserver.TypeInstantiation.Camera,
                (cameraGameObject) => m_cameraHandler = cameraGameObject.GetComponent<CameraHandler>(),
                true
            );
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(m_tagGameObjectTriggering))
            {
                Debug.Log("Changing Camera State to " + m_newState);
                m_cameraHandler.SetCameraState(m_newState);
            }
        }

        private void OnDrawGizmosSelected ()
        {
            Gizmos.DrawIcon(transform.position, "Camera.png", true, Color.green);
        }
    }
}