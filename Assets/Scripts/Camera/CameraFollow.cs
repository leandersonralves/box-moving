﻿using MainLeafTest.Handler;
using MainLeafTest.Observer.Scene;
using System;
using UnityEngine;

namespace MainLeafTest.Camera
{
    public class CameraFollow : MonoBehaviour
    {
        #region Translate Parameters
        [Header("Translate Parameters")]
        [SerializeField]
        private Vector3 m_offsetTranslate = new Vector3(0f, 3f, -10f);

        [SerializeField]
        private float m_translateSpeed = 20f;

        [SerializeField]
        private float m_translateDelayTime = 10f;
        #endregion

        #region Angular Parameters
        [Header("Angular Parameters")]
        [SerializeField]
        private float m_offsetEyeHeight = 10f;

        [SerializeField]
        private float m_maxAngularVelocity = 20f;

        [SerializeField]
        private float m_AngularDelayTime = 10f;

        [Header("Mouse Control Parameters")]
        [SerializeField]
        private float m_mouseAxisXSpeed = 10f;

        [SerializeField]
        private float m_mouseAxisYSpeed = 10f;

        [SerializeField]
        private float m_mouseMaxAngleUp = 10f;

        [SerializeField]
        private float m_mouseMaxAngleDown = -10f;

        [SerializeField]
        private float m_timeToRecenter = 2f;

        //TODO: check if is necessary a recenter.
        //private float m_nextTimeRecenter = 0f;

        private float mouseX = 0f;

        private float accumulatedMouseX = 0f;

        private float mouseY = 0f;

        private float accumulatedMouseY = 0f;

        private float m_angleToRealign = 0.25f;
        #endregion

        #region References Variables
        private Transform m_target;

        private CharacterHandler m_targetCharacterHandler;

        private Vector3 m_currentTranslateVelocity;

        private Vector3 m_currentAngularVelocity;
        #endregion

        private void Awake()
        {
            InstantionObserver.TriggerCallback(InstantionObserver.TypeInstantiation.Camera, gameObject);

            InstantionObserver.RegisterCallback(
                InstantionObserver.TypeInstantiation.Player,
                SetTarget,
                true
            );
        }

        void Update()
        {
            if (!m_target) return;

            mouseX = Input.GetAxis("Mouse X");
            mouseY = Input.GetAxis("Mouse Y");

            if (!Mathf.Approximately(0f, mouseX) || !Mathf.Approximately(0f, mouseY))
            {
                accumulatedMouseX += mouseX * m_mouseAxisXSpeed * Time.deltaTime;
                accumulatedMouseY += mouseY * m_mouseAxisYSpeed * Time.deltaTime;
                accumulatedMouseY = Mathf.Clamp(accumulatedMouseY, m_mouseMaxAngleDown, m_mouseMaxAngleUp);

                //TODO: check if is necessary a recenter.
                //m_nextTimeRecenter = Time.time + m_timeToRecenter;
            }

            float newAngleY = m_targetCharacterHandler.FowardAngle + accumulatedMouseX;
            var p2 = Quaternion.Euler(accumulatedMouseY, newAngleY, 0f) * m_offsetTranslate;

            //method using Spherical Lerp
            transform.position = Vector3.Slerp(
                transform.position, p2 + m_target.position, m_translateSpeed * Time.deltaTime
            );

            var viewDirection = -(p2 + new Vector3(0f, -m_offsetEyeHeight, 0f));
            transform.forward = Vector3.SmoothDamp(transform.forward, viewDirection, ref m_currentAngularVelocity, m_AngularDelayTime, m_maxAngularVelocity);

            if (!Mathf.Approximately(0f, mouseX))
            {
                var newDirection = m_target.position - transform.position;
                newDirection.y = 0f;
                m_targetCharacterHandler.ForceFowardDirection(newDirection);
            }
        }

        public void SetTarget (GameObject newTarget)
        {
            m_target = newTarget.transform;
            m_targetCharacterHandler = m_target.GetComponent<Handler.CharacterHandler>();
        }

        public void ResetPosition()
        {
            transform.position = Vector3.Slerp(
                transform.position, m_offsetTranslate + m_target.position, m_translateSpeed * Time.deltaTime
            );

            transform.forward = -(m_offsetTranslate + new Vector3(0f, -m_offsetEyeHeight, 0f));
            accumulatedMouseX = 0f;
            accumulatedMouseY = 0f;
            m_currentAngularVelocity = Vector3.zero;
        }

        private void OnDestroy()
        {
            InstantionObserver.UnregisterCallback(
                InstantionObserver.TypeInstantiation.Player,
                SetTarget
            );
        }
    }
}