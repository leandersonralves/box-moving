﻿using System.Collections;
using System.Collections.Generic;
using MainLeafTest.Observer.Scene;
using UnityEngine;

namespace MainLeafTest.Camera.Trigger
{
    public class TriggerRepositionCamera : MonoBehaviour
    {
        [SerializeField]
        private string m_tagGameObjectTriggering = "Player";

        [SerializeField]
        private Transform m_cameraPoint;

        private CameraFixed m_cameraFixed = null;

        private void Awake()
        {
            InstantionObserver.RegisterCallback(
                InstantionObserver.TypeInstantiation.Camera,
                (cameraGameObject) => m_cameraFixed = cameraGameObject.GetComponent<Camera.CameraFixed>(),
                true
            );
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(m_tagGameObjectTriggering))
            {
                m_cameraFixed.SetNewFixedPosition(m_cameraPoint.position);
                Debug.Log("Changing Camera Position to " + m_cameraPoint.position);
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (m_cameraPoint)
            {
                Gizmos.DrawIcon(m_cameraPoint.position, "Camera.png", true, Color.green);
            }
        }
    }
}