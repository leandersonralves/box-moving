﻿using MainLeafTest.Camera;
using UnityEngine;
using GameplayState = MainLeafTest.Controller.GameplayController.GameplayState;

namespace MainLeafTest.Handler
{
    public class CameraHandler : MonoBehaviour
    {
        public enum State
        {
            Fixed,
            Follow
        }

        [SerializeField]
        private State m_cameraState = State.Follow;

        private CameraFixed m_cameraFixed;

        private CameraFollow m_cameraFollow;

        private void Awake()
        {
            m_cameraFixed = GetComponent<CameraFixed>();
            m_cameraFollow = GetComponent<CameraFollow>();
            SetCameraState(m_cameraState);

            Controller.GameplayController.Instance.OnChangingState += OnChangingGameState;
        }

        public void OnChangingGameState (GameplayState gameState)
        {
            switch (gameState)
            {
                case GameplayState.Pausing:
                case GameplayState.Loosing:
                case GameplayState.Wining:
                    m_cameraFixed.enabled = false;
                    m_cameraFollow.enabled = false;
                    break;

                case GameplayState.Playing:
                    SetCameraState(m_cameraState);
                    break;

                case GameplayState.Restarting:
                    m_cameraFollow.ResetPosition();
                    m_cameraFixed.enabled = false;
                    m_cameraFollow.enabled = false;
                    break;
            }
        }

        public void SetCameraState (State newState)
        {
            Debug.Log("Changing Camera State to " + newState);

            m_cameraState = newState;
            switch(newState)
            {
                case State.Fixed:
                    m_cameraFixed.enabled = true;
                    m_cameraFollow.enabled = false;
                    break;

                case State.Follow:
                    m_cameraFixed.enabled = false;
                    m_cameraFollow.enabled = true;
                    break;
            }
        }
    }
}