﻿using UnityEngine;

using GameState = MainLeafTest.Controller.GameplayController.GameplayState;

namespace MainLeafTest.Mechanic
{
    public class CoinTrigger : MonoBehaviour
    {
        [SerializeField]
        private int m_amountCoins = 1;

        [SerializeField]
        private Color m_colorCoin;

        private const string PLAYER_TAG = "Player";

        private ParticleSystem m_particle;

        private Animator m_animator;

        void Awake()
        {
            m_particle = GetComponentInChildren<ParticleSystem>();
            if (m_particle)
            {
                var main = m_particle.main;
                main.startColor = m_colorCoin;
            }
            GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", m_colorCoin);
            m_animator = GetComponentInChildren<Animator>();

            Controller.GameplayController.Instance.OnChangingState += OnChangingGameState;
        }

        public void OnChangingGameState(GameState gameState)
        {
            if (gameState == GameState.Pausing)
            {
                m_animator.speed = 0f;
                m_particle.Pause(true);
            }
            else if (gameState == GameState.Playing)
            {
                m_animator.speed = 1f;
                m_particle.Play(true);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(PLAYER_TAG))
            {
                Controller.GameplayController.Instance.SumAmountCoins(m_amountCoins);
                m_particle.transform.SetParent(null);
                m_particle.Play(true);
                Destroy(gameObject);
                Destroy(m_particle, 1f);
            }
        }

        private void OnDestroy()
        {
            if (Controller.GameplayController.Instance != null)
            {
                Controller.GameplayController.Instance.OnChangingState -= OnChangingGameState;
            }
        }
    }
}