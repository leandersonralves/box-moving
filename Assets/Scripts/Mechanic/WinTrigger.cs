﻿using UnityEngine;

namespace MainLeafTest.Mechanic
{
    public class WinTrigger : MonoBehaviour
    {
        [SerializeField]
        private string m_playerTag = "Player";

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(m_playerTag))
            {
                Controller.GameplayController.Instance.ChangeState(
                    Controller.GameplayController.GameplayState.Wining
                );
            }
        }
    }
}