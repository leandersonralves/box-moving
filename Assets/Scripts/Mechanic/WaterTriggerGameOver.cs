﻿using UnityEngine;

namespace MainLeafTest.Mechanic
{
    public class WaterTriggerGameOver : MonoBehaviour
    {
        [SerializeField]
        private string m_playerTag = "Player";

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag(m_playerTag))
            {
                Controller.GameplayController.Instance.EnemyFallWater();
            }
        }
    }
}